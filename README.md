If you are accused of a criminal charge in Michigan, we can help you get the advice you need, and the opportunity to be treated fairly in court. Criminal defense is what we do. We fight for people just like you, in courts across Michigan.

Address: 36700 Woodward Ave, #107, Bloomfield Hills, MI 48304, USA

Phone: 844-342-5353

Website: https://www.mymichigandefenselawyer.com
